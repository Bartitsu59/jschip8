const fs = require("fs");
const ansi = require("node-ansi");

let chip = {
	ram:	new Uint8Array(4096),
	stack:	new Uint16Array(16),
	cpu:	{
			V0: 0,
			V1: 0,
			V2: 0,
			V3: 0,
			V4: 0,
			V5: 0,
			V6: 0,
			V7: 0,
			V8: 0,
			V9: 0,
			Va: 0,
			Vb: 0,
			Vc: 0,
			Vd: 0,
			Ve: 0,
			Vf: 0,
			I: 0,
			SP: -1,
			PC: 0x200
		}
}

let font = new Uint8Array([
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80 // F
]);

let ibmlogo = new Uint8Array([
    0xff,0x0,0xff,0x0,0x3c,0x0,0x3c,0x0,0x3c,0x0,0x3c,0x0,0xff,0x0,0xff,
    0xff,0x0,0xff,0x0,0x38,0x0,0x3f,0x0,0x3f,0x0,0x38,0x0,0xff,0x0,0xff,
    0x80,0x0,0xe0,0x0,0xe0,0x0,0x80,0x0,0x80,0x0,0xe0,0x0,0xe0,0x0,0x80,
    0xf8,0x0,0xfc,0x0,0x3e,0x0,0x3f,0x0,0x3b,0x0,0x39,0x0,0xf8,0x0,0xf8,
    0x03,0x0,0x07,0x0,0xf,0x0,0xbf,0x0,0xfb,0x0,0xf3,0x0,0xe3,0x0,0x43,
    0xe0,0x0,0xe0,0x0,0x80,0x0,0x80,0x0,0x80,0x0,0x80,0x0,0xe0,0x0,0xe0
]);

let cls = function() {
	//term.eraseDisplay();
	ansi.clear();
}

let movexy = function(x,y) {
	//term.moveTo(x,y);
	ansi.gotoxy(x,y);
}

let crlf = function() {
	//term.move(-8,1);
	ansi.left(8); ansi.down(1);
}

let draw = function(b) {
	let pattern = "";
	if (b & 0x80) pattern += "█"; else pattern += " ";
	if (b & 0x40) pattern += "█"; else pattern += " ";
	if (b & 0x20) pattern += "█"; else pattern += " ";
	if (b & 0x10) pattern += "█"; else pattern += " ";
	if (b & 0x8) pattern += "█"; else pattern += " ";
	if (b & 0x4) pattern += "█"; else pattern += " ";
	if (b & 0x2) pattern += "█"; else pattern += " ";
	if (b & 0x1) pattern += "█"; else pattern += " ";
	//term(pattern);
	ansi.write(pattern);
}

let cart = fs.readFileSync('cart.ch8', null);

chip.ram.set(font);

//console.log("starting Chip8 CPU");
//console.log("V0 value = "+chip.cpu.V0);

chip.ram.set(cart,0x200);

while(chip.cpu.PC<4096) {
	let opcode = chip.ram[chip.cpu.PC].toString(16).padStart(2,"0")+chip.ram[chip.cpu.PC+1].toString(16).padStart(2,"0");
	//console.log("opcode : "+opcode);
	if(opcode == "00e0") cls();  // CLS
	else if(opcode == "00ee") { chip.cpu.PC = chip.stack[chip.cpu.SP]; chip.cpu.SP--; }  // RET
	else if(opcode == "00fd") break;  // EXIT
	else if(opcode.startsWith("1")) {	// JP addr
		let val_vh = opcode[1];
		let val_h = opcode[2];
		let val_l = opcode[3];
		chip.cpu.PC = parseInt(val_vh,16)*256+parseInt(val_h,16)*16+parseInt(val_l,16);
		continue;
	} else if(opcode.startsWith("2")) {	// CALL addr
		let val_vh = opcode[1];
		let val_h = opcode[2];
		let val_l = opcode[3];
		chip.cpu.SP++;
		chip.stack[chip.cpu.SP]=chip.cpu.PC;
		chip.cpu.PC = parseInt(val_vh,16)*256+parseInt(val_h,16)*16+parseInt(val_l,16);
		continue;
	} else if(opcode.startsWith("3")) {	// SE VX,NN
		let rnum=opcode[1];
		let val_h = opcode[2];
		let val_l = opcode[3];
		if (chip.cpu["V"+rnum] == parseInt(val_h,16)*16+parseInt(val_l,16)) chip.cpu.PC +=2;
	} else if(opcode.startsWith("4")) {	// SNE VX,NN
		let rnum=opcode[1];
		let val_h = opcode[2];
		let val_l = opcode[3];
		if (chip.cpu["V"+rnum] != parseInt(val_h,16)*16+parseInt(val_l,16)) chip.cpu.PC +=2;
	} else if(opcode.startsWith("5")) {	// SE VX,VY
		let rnum1=opcode[1];
		let rnum2 = opcode[2];
		if (chip.cpu["V"+rnum1] == chip.cpu["V"+rnum2]) chip.cpu.PC +=2;
	} else if(opcode.startsWith("6")) {	// LD VX,NN
		let rnum=opcode[1];
		let val_h = opcode[2];
		let val_l = opcode[3];
		chip.cpu["V"+rnum] = parseInt(val_h,16)*16+parseInt(val_l,16);
	} else if(opcode.startsWith("7")) {	// ADD VX,NN
		let rnum=opcode[1];
		let val_h = opcode[2];
		let val_l = opcode[3];
		chip.cpu["V"+rnum] += parseInt(val_h,16)*16+parseInt(val_l,16);
		chip.cpu["V"+rnum] &= 255;
	} else if(opcode.startsWith("8") && opcode[3] == 0) {	// LD VX,VY
		let rnum1 = opcode[1];
		let rnum2 = opcode[2];
		chip.cpu["V"+rnum1] = chip.cpu["V"+rnum2];
	} else if(opcode.startsWith("8") && opcode[3] == 1) {	// OR VX,VY
		let rnum1 = opcode[1];
		let rnum2 = opcode[2];
		chip.cpu["V"+rnum1] |= chip.cpu["V"+rnum2];
	} else if(opcode.startsWith("8") && opcode[3] == 2) {	// AND VX,VY
		let rnum1 = opcode[1];
		let rnum2 = opcode[2];
		chip.cpu["V"+rnum1] &= chip.cpu["V"+rnum2];
	} else if(opcode.startsWith("8") && opcode[3] == 3) {	// XOR VX,VY
		let rnum1 = opcode[1];
		let rnum2 = opcode[2];
		chip.cpu["V"+rnum1] ^= chip.cpu["V"+rnum2];
	} else if(opcode.startsWith("8") && opcode[3] == 4) {	// ADD VX,VY
		let rnum1 = opcode[1];
		let rnum2 = opcode[2];
		let result = chip.cpu["V"+rnum1] + chip.cpu["V"+rnum2];
		if (result > 255) chip.cpu.VF = 1; else chip.cpu.VF = 0;
		chip.cpu["V"+rnum1] = result & 255;
	} else if(opcode.startsWith("8") && opcode[3] == 5) {	// SUB VX,VY
		let rnum1 = opcode[1];
		let rnum2 = opcode[2];
		if (chip.cpu["V"+rnum1] > chip.cpu["V"+rnum2]) chip.cpu.VF = 1; else chip.cpu.VF = 0;
		let result = chip.cpu["V"+rnum1] - chip.cpu["V"+rnum2];
		chip.cpu["V"+rnum1] = result & 255;
	} else if(opcode.startsWith("8") && opcode[3] == 6) {	// SHR Vx {,VY}
		let rnum1 = opcode[1];
		let rnum2 = opcode[2];
		if (chip.cpu["V"+rnum1] & 0x01) chip.cpu.VF = 1; else chip.cpu.VF = 0;
		chip.cpu["V"+rnum1] >>= 1;
	} else if(opcode.startsWith("8") && opcode[3] == "e") {	// SHL Vx {,VY}
		let rnum1 = opcode[1];
		let rnum2 = opcode[2];
		if (chip.cpu["V"+rnum1] & 0x80) chip.cpu.VF = 1; else chip.cpu.VF = 0;
		chip.cpu["V"+rnum1] <<= 1;
		chip.cpu["V"+rnum1] &= 255;
	} else if(opcode.startsWith("9")) {	// SNE VX,VY
		let rnum1=opcode[1];
		let rnum2 = opcode[2];
		if (chip.cpu["V"+rnum1] != chip.cpu["V"+rnum2]) chip.cpu.PC +=2;
	} else if(opcode.startsWith("a")) {	// LD I,addr
		let val_vh = opcode[1];
		let val_h = opcode[2];
		let val_l = opcode[3];
		chip.cpu.I = parseInt(val_vh,16)*256+parseInt(val_h,16)*16+parseInt(val_l,16);
	} else if(opcode.startsWith("c")) {	// RND Vx,byte
		let rnum=opcode[1];
		let val_h = opcode[2];
		let val_l = opcode[3];
		chip.cpu["V"+rnum] = Math.random() * 255 & (parseInt(val_h,16)*16+parseInt(val_l,16));
	} else if(opcode.startsWith("d")) {			// DRAW x,y,n
		let rnumx = opcode[1];
		let rnumy = opcode[2];
		let n = parseInt(opcode[3],16);
		movexy(chip.cpu["V"+rnumx],chip.cpu["V"+rnumy]);
		for(let i=0;i<n;i++) {
		  draw(chip.ram[chip.cpu.I+i]);
		  crlf();
		}
	} else if(opcode.startsWith("f") && opcode.substring(2,4) == "1e") {	// ADD I,Vx
		let rnum = opcode[1];
		chip.cpu.I += chip.cpu["V"+rnum];
	} else if(opcode.startsWith("f") && opcode.substring(2,4) == "29") {	// LDSPR s
		let s = opcode[1];
		chip.cpu.I = chip.cpu["V"+s]*0x5;
	} else if(opcode.startsWith("f") && opcode.substring(2,4) == "33") {	// LD B, Vx
		let rnum = opcode[1];
		chip.ram[chip.cpu.I] = Math.floor(chip.cpu["V"+rnum]/100);
		chip.ram[chip.cpu.I+1] = Math.floor(chip.cpu["V"+rnum]/10)%10;
		chip.ram[chip.cpu.I+2] = chip.cpu["V"+rnum]%10;
	} else if(opcode.startsWith("f") && opcode.substring(2,4) == "55") {	// LD [I],VX
		let rnum = opcode[1];
		for(let i=0;i<=rnum;i++) {
		  chip.ram[chip.cpu.I+i] = chip.cpu["V"+i];
		}
	} else if(opcode.startsWith("f") && opcode.substring(2,4) == "65") {	// LD Vx,[I]
		let rnum = opcode[1];
		for(let i=0;i<=rnum;i++) {
		  chip.cpu["V"+i] = chip.ram[chip.cpu.I+i];
		}
	} else if(opcode=="0000") {	// NOP
	} else {
		console.log("Opcode %s not implemented", opcode);
		break;
	}
	chip.cpu.PC += 2;
}
//console.log("V0 value = "+chip.cpu.V0.toString(16));
//console.log("Ve value = "+chip.cpu.Ve.toString(16));
